# TTR- und andere Werte

## Definitionen

### TTR

https://de.wikipedia.org/wiki/Tischtennis-Rating

> Das Tischtennis-Rating (kurz TTR) ist ein Ende 2010 veröffentlichtes Wertungssystem für die individuelle Spielstärke jedes deutschen aktiven Tischtennis-Spielers. Das System wurde grob nach dem Vorbild der im Schach genutzten Elo-Zahl entworfen. Die Wertung erfolgt übergreifend über alle Regionen, Altersstufen und Geschlechter, es gibt also nur eine TTR-Rangliste. Jeder Spieler besitzt zwei Wertungszahlen: den TTR-Wert und den davon abgeleiteten QTTR-Wert (Quartals-TTR-Wert).
>
> Jeden Tag erfolgt automatisiert eine Aktualisierung der TTR-Werte, indem alle an diesem Tag neu im Portal click-TT eingetragenen Spielergebnisse chronologisch abgearbeitet werden. Die QTTR-Werte werden hingegen nur quartalsweise aktualisiert und sind die zu bestimmten Stichtagen (11.2., 11.5., 11.8. und 11.12.) „eingefrorene“ TTR-Werte. Der QTTR-Wert wird für die Reihenfolge bei Mannschaftsaufstellungen und für die Klasseneinteilung bei manchen Turnieren herangezogen.

TTR-Rangliste: https://www.mytischtennis.de/public/ranking

### Live-PZ

https://www.bettv.de/wp-content/uploads/2017/12/Veroeffentlichung.pdf

> Die LivePZ wird exakt so berechnet wie der TTR-Wert im click-tt. Beide Werte sind dennoch nicht exakt vergleichbar, da abweichende Startwerte zu Grunde liegen. Eine Zusammenführung und Vereinheitlichung wäre technisch möglich, ist aber nicht realistisch.
