# Aufgaben

## Laufende Arbeiten über ein Spieljahr

1. Teilnahme an Norddeutschen und Deutschen Jugendwartetagungen, LSB-Veranstaltungen und Präsidiumssitzungen
2. Einladung aller JA- und Präsidiumsmitglieder + Landestrainer zu den JA-Sitzungen
3. Organisation und Durchführung des Punkt- und Pokalspielbetriebes
4. Turniervergabe, -ausschreibungen, Organisation/Durchführung von Berliner Einzelturnieren insbes. VRL, LRL, BEMs
5. Ergebnisverarbeitung in tt-live und click-tt, Ergebnisveröffentlichung
6. Zusammenarbeit mit Landestrainer/Verbandstrainer
	1. Halbjährliche Zusammenstellung der Verbandskaderlisten
	2. Hilfe bei der Traineraus/-fortbildung
	3. Hilfe bei Sichtungsmaßnahmen des Verbandes
	4. Nominierung von Nachwuchsspieler/innen zu überregionalen Wettkämpfen
	5. Teilnahmemeldung und Organisation überregionaler Einzel- und Mannschaftsturniere
	6. Organisation und Durchführung mindestens einer Sichtungsmaßnahme im Jahr
	7. Hilfe bei der Findung von Betreuern für überregionale Veranstaltungen
	8. Regelmäßige Führung von Anwesenheitslisten (Spieler/Trainer/Sparringspartner) im LZ
7. Minimeisterschaften Ortsentscheide/Landesfinale
8. Landesfinals/Bundesfinale JtfO
9. Regelmäßige und zeitgemäße Anpassung der JO, JSpO und JTurnO
10. Öffentlichkeitsarbeit (Berichte/Fotos) zu Berliner und überregionalen Veranstaltungen
11. Zusammenarbeit und Kommunikation mit allen Vereinsvertretern

## Projekte für die nächsten 2 Jahre

1. Erarbeitung eines Grundkonzeptes zur mittel-/langfristigen Entwicklung der Berliner TT-Jugend auf allen Leistungsebenen
2. Überarbeitung des LZ-Statuts
3. möglichst regelmäßiger Workshop "Mittmischer" (Gewinnung Jugendlicher fürs Ehrenamt)
4. Hilfe bei der Gründung von neuen Jugendabteilungen (insbesondere in Vereinen mit überregionalen Spielklassen)
5. Erarbeitung und Veröffentlichung eines Leitfadens für Vereinstrainer
6. Unterstützung von Vereinen mit Mädchen-Trainingsgruppen
7. Gerechtere Einstufung von LPZ-Anfangswerten über tt-live
8. Organisation und Durchführung Norddeutsche Einzelmeisterschaften Jugend 18/15 am 28./29.Januar 2023
