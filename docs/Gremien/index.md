# Gremien

> Die zu wählenden Mitglieder des Präsidiums, der Ausschüsse, der Ressorts sowie der Rechtsprechungs- und Kontrollorgane werden für zwei Jahre gewählt, gerechnet von der Annahme der Wahl bis zur Neuwahl.
