# Aufgaben

1. Zusammenarbeit mit Jugendausschuss
	1. Halbjährliche Zusammenstellung der Verbandskaderlisten
	2. Hilfe bei der Traineraus/-fortbildung
	3. Hilfe bei Sichtungsmaßnahmen des Verbandes
	4. Nominierung von Nachwuchsspieler/innen zu überregionalen Wettkämpfen
	5. Teilnahmemeldung und Organisation überregionaler Einzel- und Mannschaftsturniere
	6. Organisation und Durchführung mindestens einer Sichtungsmaßnahme im Jahr
	7. Hilfe bei der Findung von Betreuern für überregionale Veranstaltungen
	8. Regelmäßige Führung von Anwesenheitslisten (Spieler/Trainer/Sparringspartner) im LZ
2. Zusammenarbeit und Kommunikation mit allen Vereinsvertretern

## Satzung

1. beratendes Mitglied des Präsidiums
2. Mitglied des Sportausschuss
3. Nominierungen
	1. Bei Verzicht eines qualifizierten Spielers für das überregionale Turnier entscheidet der Landestrainer mit dem Jugendausschuss über den Einsatz eines Nachrückers oder die Möglichkeiten eines Platztausches mit einem anderen Verband.
	2. Vorschläge für Verfügungsplätze, Freistellungen zu Qualifikationen oder Meisterschaften und Ranglisten der Jugend, sowie Vorschläge an den Sportausschuss für die Vergabe von Verfügungsplätzen von Jugendlichen bei Einzelwettbewerben der Erwachsenen im BTTV und NTTV erstellt der Landestrainer bzw. dessen Vertreter in Zusammenarbeit mit dem Jugendausschuss.

## Anlage Arbeitsvertrag

- mindestens 2x pro Jahr Anwesenheitspflicht bei Präsidiumssitzungen

Aufgaben, Kompetenzen

1. sportliche Leitung des Leistungszentrums des Berliner Tisch-Tennis Verbandes und die damit verbundene Erarbeitung und Umsetzung eines Leistungssportkonzeptes für den Berliner Tischtennissport.
2. Die Planung und Umsetzung der Schulungs-, Trainings- und Wettkampfprogramme für die im Leistungszentrum bzw. in entsprechenden Stützpunkten trainierenden Kader-AthletInnen und Talentgruppen.
3. Der Arbeitnehmer hat das halbjährliche Vorschlagsrecht gegenüber des Jugendausschuss zur Berufung von SportleriInnen in den Landeskader. Die Vorschläge basieren auf Grundlage des Statutes des Leistungszentrums und einer sportfachlichen Bewertung.
4. Eine halbjährliche sportliche Bewertung der KaderathletInnen und eine schriftliche Analyse der einzelnen AthletInnen über deren leistungssportliches Entwicklungspotential.
5. Die Wettkampfbetreuung bei überregionalen Turnieren, bei denen vom Tischtennisverband nominierte AthletInnen eingesetzt sind. Die Betreuung von Erwachsenen auf überregionalen Veranstaltungen ist optional.
6. Die Erarbeitung von Grundsätzen und Kriterien sowie Mitwirkung bei der Talentsuche, -sichtung und -förderung
7. Die Zusammenarbeit mit den Sportschulen des Landessportbundes Berlin und mit den sportbetonten Schulen
8. Die Erarbeitung und Fortführung der jeweiligen Rahmentrainingspläne der Kaderathleten und weiteren Auswahlspieler.
9. Eine enge Zusammenarbeit mit Tischtennis-Trainern weiterer Berliner Spitzenverbände sowie der durch den BTTV eingesetzten Honorartrainern.
10. Die Erarbeitung eines Tätigkeitkeitsberichtes jeweils zum 15.01. und 15.07. der abgeschlossenen Periode / Saison.
11. Besuch der Fortbildungslehrgänge und -seminare des DTTB im Rahmen der A-Lizenz-Vorgaben.
12. Erledigung aller mit den vorgenannten Aufgaben im Zusammenhang stehenden Verwaltungs- und Vorbereitungsarbeiten, insbesondere Nominierungen zu überregionalen Wettkämpfen.
13. Die Teilnahme an Präsidiumssitzungen sowie Jugendausschusssitzungen auf Einladung
14. Dauerhafte Führung von Anwesenheitslisten im Leistungszentrum von Spielern, Trainem sowie Gästen. Diese sind der Geschäftsstelle zu übermitteln. Unter Berücksichtigung der Corona-Situation müssen von allen Personen die Kontaktdaten erhoben werden und zur Aufbewahrung der Geschäftsstelle übergeben werden.
15. Mitarbeit an der Öffentlichkeitsarbeit und an Event- und Promotionsmaßnahmen.


## Aktuelles

- Kommunikation mit Eltern, Verbänden, Präsidium
- Abrechnungen von Leistungen
- Berichte von Lehrgängen, Turnieren etc.

## Plan

- Abstimmung mit Jugendausschuss, Landestrainer, Eltern, Vereinen
- Trainer in Vereinen werben, sichten und LZ abstimmen
- Ziele benennen
	- Landesleistungszentrum/Landesstützpunkt
	- x Schulen bespielen
- neues LZ-Statut
- daraus Zeitplan und Finanzplan ableiten
	- Anzahl der Trainerstellen
	- Turnierkosten
	- Selbstbeteiligung der Teilnehmenden, falls nötig
