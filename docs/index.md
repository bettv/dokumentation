# Willkommen zur Dokumentation

![Logo – BeTTV](images/logo.png){ align=right }

!!! warning ""
	Das ist die unfertige Beta!

!!! tip ""
	Nix mehr vergessen im Verband.

Das ist der Versuch, den Verband zu dokumentieren:

- Welche Gremien gibt es?
- Wer macht was?
- Wen spreche ich an?
- Wie organisiere ich ein Turnier?
- Wer ist für die Staffeleinteilung zuständig?
- Und überhaupt?
