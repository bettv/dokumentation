# Freizeit- und Breitensportausschuss

Der Freizeit- und Breitensportausschuss dient dazu, auch im Freizeitbereich rund um Tischtennis schöne Angebote des Verbands zu organisieren.

Die Hoffnung besteht, dass so:

1. mehr Leute Tischtennis spielen
2. mehr Leute organisiert Tischtennis spielen, also in den Verband eintreten

Hauptaufgaben sind:

- Kommunikation mit Breitensportinitiativen
- Organisation von Freizeitveranstaltungen
- Förderung von Tischtennis außerhalb des Verbandssports
- Absprache mit dem TTT-Team

Der Freizeit- und Breitensportausschuss besteht laut Satzung aus einem/einer Referent:in, der/die den Ausschuss leitet und zwei Beisitzer:innen.


## Aufgaben laut Satzung

Rechtsgrundlage: Satzung § 20

> Der Freizeit- und Breitensportausschuss tritt nach Bedarf zusammen und wird von seinem Vorsitzenden einberufen.
> Seine Aufgaben bestehen in Initiativen im Freizeitsport 


## Zeitaufwand

- je nachdem, wie viel man machen möchte


## Ideen

- Steinplattenturniere
- virtuelle TT-Turniere
- Meet-The-Stars (eastside oder Berliner Meister:innen)
- soziale Kanäle bespielen
- gemeinsame Aktionen mit Freizeit- und Kirchenliga
- Gesundheitssport
- TT-Stars einladen
- Midi- und Miniturniere
- Auftritt bei Messen, Veranstaltungen des Sports
- sportartübergreifende Veranstaltungen