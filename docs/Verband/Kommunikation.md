# Kommunikation

[TOC]

## Rahmenbedingungen

- niedrigschwellig
- mehrsprachig (de, en, ?)

## statische Informationen

- Verbandsstruktur
	- Funktionäre
		- Aufgaben, Arbeitsumfang
	- Ausschüsse
	- Ressorts
- Themen und Ansprechpartner für Themen
	- Kinderschutz
	- Leistungssport
	- Leistungszentrum
	- Frauen
	- Senior:innen
	- Spielbetrieb
	- Schiedsrichter:innen
	- DTTB, LSB, Land Berlin
- Vereine
	- filterbar nach Geschlecht, Sprache, Angebot, Barrierefreiheit, ...
	- Karte
	- Verlinkungen
	- Infoseite pro Verein mit Kontaktmöglichkeit oder Link
- How-Tos
	- Turnierorga
- Geschichte

### Technik

- Webseite
- wenn möglich, strukturierte Informationen per REST-API

## dynamische Informationen

- News
- Newsletter abonnierbar
	- alle
	- themenbezogen
- Pflichtkommunikation an Vereine
	- Newsletter oder E-Mail
	- Einladungen Verbandstag o.ä.
	- Mitteilungen des Präsidiums
- Termine
	- themenbezogen
	- filterbar
	- abonnierbar

### Technik

- RSS-Feed
- Newsletter-Mailings
- Kalender mit abonnierbarem ical


## Sport- und Spielbetrieb

- Berliner Ligen
- Turniere
- Freizeitliga
- Kirchenliga
- Freizeitsport

### Technik

- wenn möglich, per REST-API


## Vernetzung

- Diskussion
- gegenseitige Hilfe

### Technik

- Mastodon
- Twitter
- Facebook
- BTTV-Webseite
