# Aufgaben

!!! warning "Zu klären, ob nötig"
    - Barkasse?
    - Kopiervorgänge
    - Anlage und Überwachung von Termingeldern
    - Allgemeine Verwaltungs- und Sekretariatsaufgaben
    - Informieren des Präsidiums und der Funktionäre des BTTV
    - Umsetzung von Beschlüssen des Verbandstags
    - Entwerfen von Flyern und Verbandsnachrichten
    - Vereine (in Zusammenarbeit mit Präsidium)
        - Mitgliedskündigungen verfassen
        - Mitgiedsneuaufnahmen Regularien checken

Aufgabe der Geschäftsstelle ist die Organisation des vollständigen Geschäftsstellenbetriebs.

## Geschäftsstellenbetrieb

- Ansprechpartner des Verbands
    - vor Ort (Öffnungszeiten)
    - telefonisch
    - per E-Mail
    - postalisch
- aktiver Kontakt mit Vereinen, der Senatsverwaltung, LSB und übergeordneten Verbänden
- selbstständige Planung und Organisation von Veranstaltungen in und außerhalb Berlins
- Pflege und Veröffentlichung der Dokumentvorlagen
- Urkunden erstellen
- Lizenzen ausstellen / Lizenzpflege (Übungsleiter:innen, Trainer:innen)
- Veranstaltungsunterstützung: planen, organisieren, durchführen
    - Verbandstag
    - Turniere
    - Sitzungen
    - Feiern


## Finanzen

- Durchführung des Zahlungsverkehrs
- Einkauf von Materialien (im Wert bis zu 500 € auch ohne Zustimmung des Präsidiums)
- Buchführung
    - Führung der Verbandsbuchhaltung
    - Überweisungen
    - Buchungen
    - Abrechnungen (JtfO, Mitarbeiter, LSB, ...)
    - Belege zuordnen / einordnen
    - Barkasse
    - Reisekostenabrechnung
- Rechnungswesen
    - Erstellung und Bearbeitung von Ein- und Ausgangsrechnungen
    - Rechnungsbegleichung
    - Beitragseingänge überwachen
    - Mahnungen schreiben
- Beantragung und Abrechnung von Zuwendungen
- Liquiditätskontrolle
- Kontoführung
- Vertragsangelegenheiten
- LSB -- Geldeingänge, Kontakte, Zuschüsse
- Geschäftsverbindungen pflegen (Hotels, Sponsoren, andere Firmen)
- Lohnbuchhaltung: Gehälter (Sozialversicherung, Steuern, Beiträge) über- bzw. anweisen
- Kontierung
- Erstellen von Tabellen, Bilanzen, Prognosen, ... (Cashflow, Controlling, ...)
- Termingelder
- Vertragsgelder, Geldein- und ausgänge
- Info an Vizepräsident Finanzen


## Sport

- Erstellung des Jahresterminplans
- Terminierung der Hallen bzw. Zeiten
- Pflege der Portale tischtennislive und click-TT/NuLiga
- Vereine (in Zusammenarbeit mit Präsidium)
    - Mitgliedskündigungen verfassen
    - Mitgiedsneuaufnahmen Regularien checken

### Mannschaftsspielbetrieb

- Pflege des Jahreskalenders
- Durchführung der An-/Um- und Abmeldung von Spieler:innen

### Turniere

- Pflege des Turnierkalenders
- Verwaltung der Turniernummern
- fachliche und organisatorische Unterstützung bei der Turnierplanung, -durchführung und -abrechnung
- Verwaltung und Ausgabe des Turniermaterials
    - Tische
    - Netze
    - Banden
    - Turniersets
- Hilfe bei und Klärung von Transportfragen (JtfO, Verbandsmaterial, ...)


## Rechtliches

- Vorbereitungen bei Vereinsregisteränderungen
- Information über Satzungs- oder Ordnungsfragen
- Unterstützung bei Änderung von Satzung, Ordnungen etc.


## Technik

- Verwaltung aller Onlinekonten des Verbands
    - E-Mail
    - Server
    - Webseite
    - tischtennislive
    - clickTT
- Anschaffung, Erfassung und Information über notwendige Neuanschaffungen oder Updates
    - Hard- und Software
    - Büroausstattung
    - Büromaterial
- Pflege der Homepage


## Aufgaben für das Präsidium

- Protokollablage und -verteilung
- Weiterleitung von E-Mails zur Klärung
- Verbandstags-Vorbereitung
    - Raumbuchung
    - Erinnerung an Termine und Formalien
    - Hilfe bei Tagesordnung
    - Antragsheft
    - Berichtsheft
    - Ermittlung der Stimmenzahl
    - Einladung
- Verbandstags-Durchführung
    - Teilnahme
    - Protokoll
    - Stimmenzählung


## Aufgaben für die Ausschüsse

- Protokollablage
- Unterstützung bei Veranstaltungsorganisation (Lehrgänge, Treffen, Sitzungen, ...)
    - Ankündigungen auf der Homepage
    - organisatorische Hilfe bei der Durchführung
    - Verteilung und Ablage von Ergebnissen
