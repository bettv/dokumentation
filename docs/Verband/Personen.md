# Wer arbeitet für uns?

## Arbeitsvertrag

### Geschäftsstelle

#### Kontakt

- Paul-Heyse-Str. 29, 10407 Berlin
- +49 30 8929176
- <geschaeftsstelle@bettv.de>

### Leistungszentrum

- Verbandstrainer
- 50/50-Trainer Inklusion

## Vertrag

- Notar
- Webseite
- Server
- Steuerberater
- Lohnabrechnung
