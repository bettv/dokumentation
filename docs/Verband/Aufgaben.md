# Aufgaben

## laut Satzung

- die Pflege und Förderung des Tischtennissports in Berlin als Breiten-, Schul-, Leistungs- und Spitzensport, wobei es insbesondere die Jugend für diese Sportart zu gewinnen gilt,
- die Vertretung des Berliner Tischtennissports im In- und Ausland gegenüber dem Deutschen Tischtennis Bund (DTTB), den Fachverbänden des Berliner Sports, dem Landessportbund sowie den Medien,
- die Erteilung der Spielererlaubnis für Vereine, Abteilungen, Mannschaften und Spieler,
- die Schaffung, Weiterbildung und Überwachung aller für die Verwaltung und spieltechnische Abwicklung der Verbandsaufgaben erforderlichen Ordnungen und übriger Bestimmungen, stets unter Berücksichtigung internationaler Tischtennisregeln und der Wettspielordnung des DTTB,
- die Schlichtung von Streitigkeiten zwischen Mitgliedern des BTTV,
- die Ahndung von Verstößen gegen Bestimmungen des DOSB, des DTTB und des BTTV,
- die Veranstaltung von Mannschafts- und Einzelwettbewerben sowie die Ausrichtung von ihm durch den DTTB übertragenen Veranstaltungen,
- die Nominierung seiner Spieler sowie Mannschaften zu Veranstaltungen des DTTB und die Teilnahme seiner Auswahlmannschaften an nationalen und internationalen Vergleichen,
- die Initiierung von Maßnahmen zum Schutz der Kinder und Jugendlichen vor jeder Art von Gewalt und Missbrauch sowie die konsequente Ahndung von Verstößen.
