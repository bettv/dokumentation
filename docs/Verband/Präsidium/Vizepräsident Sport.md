# Vizepräsident:in Sport

Der/die Vizepräsident:in Sport koordiniert den Sportausschuss und ist damit Ansprechpartner:in für alle sportlichen Belange.

Er/sie wird in der Arbeit durch den Sportausschuss fachlich unterstützt, eigene fachliche Expertise ist hilfreich aber nicht notwendige Voraussetzung.  

Hauptarbeiten sind:

- Kommunikation mit dem Verband und den Vereinen
    - Teilnahme an Präsidiumssitzungen und Koordination mit dem Präsidium
    - Kommunikation von Entscheidungen des Sportausschusses  
    - Postein- und -ausgang des Sportausschusses
- Koordination des Sportausschusses (Sitzungen, Protokolle, schauen, dass nix liegenbleibt)
    - Einberufung, Leitung und Dokumentation der Sportausschussitzungen
    - Überwachung der Protokollierung
    - Termin- und Entscheidungsüberwachung
- Koordination des Turnierbetriebs
    - Turnierkalender im Auge behalten
    - Turnierorga mit Geschäftsstelle koordinieren
    - Antworten zu Sport- und Turnierfragen abstimmen
- Teilnahme an den entsprechenden Ausschuss- und Wartetagungen

Der Sportausschuss ist fachlich exzellent besetzt, die Hauptaufgabe ist, alle ein- und ausgehenden Mails zu koordinieren sowie an alle wichtigen Termine zu denken. 

Die Schiedsrichter:innen organisieren sich stark selbst, hier ist kaum Arbeit zu leisten.


## Aufgaben laut Satzung

Rechtsgrundlage: Satzung § 12 (7)

> Der Vizepräsident Sport ist zuständig für den Sportbetrieb, die Ausrichtung überregionaler Veranstaltungen und das Schiedsrichterwesen des BTTV.



## Zeitaufwand

### Kommunikation und Koordination

- ein- und ausgehende Kommunikation 2-4 h pro Woche
    - Anfragen an Sportausschuss (hauptsächlich Nominierungen, Verfügungsplätze, Turniergenehmigungen)
    - Entscheidungsfindung im Sportausschuss (über Mail und Signal,)
- Berichte an Sportwartetagung, Verbandstag, 1x jährlich

### Sitzungen

- DTTB
    - Sportwartetagung des DTTB (1x pro Jahr, Fr Abend - Sa Mittag, in Frankfurt/M)
- NTTV
    - Sportwartetagung des NTTV (1x pro Jahr, ein Nachmittag im Rahmen der Norddt. Meisterschaft)
    - Ausrichtung der Sportwartetagung, wenn dem BTTV die Norddeutschen zufallen
- BTTV
    - Präsidiumssitzungen (monatlich Präsenz oder virtuell 4 h)
    - Sportausschusssitzungen (ca. vierteljährlich, be Bedarf, Einberufung, Protokoll)
    - Sportwartetagung des BTTV (1x pro Jahr, Einberufung, Protokoll)
    - Verbandstag (1x jährlich 4 h)

### Turniere

#### offizielle BTTV-Turniere

Ausrichtung sicherstellen

- Berliner Einzelmeisterschaft BEM (Januar)
- Berliner Einzelmeisterschaft der Leistungsklassen BEM/LK
- 1./2. Vorrangliste VRL, Landesrangliste LRL
- Pokal/VK (Quali)
- Berliner Pokal
- Relegation (Verantwortung des Spielausschusses)

#### optionale BTTV-Turniere

Freiwillige finden und unterstützen

- TTT
- Bären-Cup

#### überregionale Turniere

Betreuung bzw. Delegationsleitung sichern

- Norddeutsche D/H
- ggf. Deutsche D/H
- Pokal/VK
- DEM/LK

#### Vereinsturniere

Koordination der Genehmigung und bei Bedarf fachliche/technische Unterstützung im Vorfeld

- auf Antrag der Vereine, 10-20 pro Jahr
