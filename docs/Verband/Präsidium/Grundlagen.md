# Grundlagen

Rechtsgrundlage: Satzung § 12

Amtszeit: zwei Jahre, gerechnet von der Annahme der Wahl bis zur Neuwahl

!!! important ""
    Das Präsidium muss beim Amtsgericht engetragen werden (erledigt der Notar).
    Neu gewählte Mitglieder sind allerdings sofort nach der Wahl im Amt, nicht erst nach der Eintragung.

## Mitglieder

1. [Präsident:in](../Präsident/)
2. Vizepräsident:in Finanzen
3. Vizepräsident:in für Bildung und Sportentwicklung
4. [Vizepräsident:in Sport](../Vizepräsident Sport/)
5. Vizepräsident:in Jugend
6. Vizepräsident:in für Öffentlichkeitsarbeit
7. Vizepräsident:in Leistungssport

## Außerordentliche Mitglieder

"Beratende Stimmen"

1. Verbandstrainer:in
2. die Geschäftsführerin/der Geschäftsführer (haben wir im BTTV nicht, wir haben "nur"Mitarbeiter:innen in der Geschäftsstelle, ist eine Satzungsfrage)
3. Ehrenpräsident:innen
