# Präsidium

!!! info ""
    Das Präsidium heißt woanders (und im Recht) "Vereinsvorstand" oder nur "Vorstand".

Im BTTV haben wir uns dafür entschieden, das Präsidium im Ehrenamt auszüben und damit keinen "Vorstand" zu haben.
