# Präsident:in

Der/die Präsident:in repräsentiert den Verband nach innen und nach außen, kommuniziert und koordiniert alle Arbeiten.

Hauptarbeiten sind:

- offizieller Repräsentant des Verbands gegenüber DTTB, LSB, DOSB, anderen Verbänden, Senat, ...
- Personalverantwortung für die Mitarbeiter:innen des Verbands: Geschäftsstelle sowie Verbandstrainer
- Koordination des Verbands, Entscheidungen treffen
- Koordination eingehender E-Mails, deren Empfänger:in unklar ist
- Klärung von Problemen unter Einbeziehung des Präsidiums, der Geschäftsstelle und der Ausschüsse
- Verwaltung: Dinge strukturiert angehen, Termine koordinieren, mit den Ausschüssen absprechen
- Kommunikation: in den Verband hören, DTTB, LSB etc., überall dort reden, wo es nötig ist

An sich ist nicht viel inhaltlich zu tun, wenn alles läuft, das erledigen Geschäftsstelle, Vizepräsident:innen und Ausschüsse.
Allerdings ist administrativ ein hoher Aufwand zu leisten.

Man muss sehen, dass alles läuft, mit wem man redet, wer wo vertreten werden muss.


## Aufgaben laut Satzung

Rechtsgrundlage: Satzung § 12 (4)

### Repräsentation

Der Präsident repräsentiert den BTTV.

### Versammlungsvorsitz

Er führt den Vorsitz bei allen Versammlungen.

### Einberufung Verbandstag und Präsidiumssitzungen

Er beruft den Verbandstag und die Sitzungen des Präsidiums ein, stellt ihre Tagesordnungen auf und führt den Vorsitz.

### Einsicht Geschäftsführung Präsidium

Er hat das Recht, Einsicht in die Geschäftsführung eines jeden Mitglieds des Präsidiums zu nehmen.

### Gnadenrecht

Er übt das Gnadenrecht aus.


## Zeitaufwand

### Einarbeitung

- 3 h pro Tag, 6 Tage/Woche, 2 Monate
- 2-wöchentliche Präsidiumssitzungen
- Gespräche mit vielen Leuten
- Einarbeitung in alle möglichen Themen

### Nach der Einarbeitung

- DTTB
    - Vorbereitung Bundestag/-rat 2-3 Abende
    - Bundestag und Bundesrat in Präsenz, zwei Wochenenden November/April
    - Kommunikation per E-Mail bei Bedarf
    - bei Interesse Mitarbeit in Gremien/Ausschüssen oder als Posten
- NTTV
    - Verbandstag einmal jährlich Wochenende im April/Mai
    - bei Interesse Mitarbeit in Gremien/Ausschüssen oder als Posten
- BTTV
    - Verbandstag 1x jährlich 6 h + Vorbereitung
    - Präsidiumssitzungen monatlich Präsenz oder virtuell 4 h
    - Geschäftsstellenabstimmung monatlich Präsenz 3 h
    - Geschäftsstellen-Mitarbeiter:innen-Gespräche 1-2x jährlich je 1 h
    - Pokalübergabe bei Turnieren
    - Grußworte bei Feiern (mündlich und schriftlich)
- LSB
    - Vorbereitungstreffen der Präsidentinnen und Präsidenten 1x jährlich Novemberabend
    - Verbandstag 1x jährlich Novemberabend
    - Eröffnung Sportjahr Berlin 1x jährlich ?-Abend
    - Sommerfest 1x jährlich Maiabend
- Vereine
    - bei Bedarf und Einladung