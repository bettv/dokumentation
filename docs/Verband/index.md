# Verband

!!! tip ""
    Der Berliner Tisch-Tennis Verband e.V. (BTTV) ist der Zusammenschluss der ca. 100 Tischtennisvereine und Tischtennissparten in der Hauptstadt Berlin mit circa 7.000 Mitgliedern.
    Die ehrenamtliche Verbandsarbeit wird durch eine hauptamtliche Geschäftsstelle unterstützt.
    Von der Freizeitliga bis zur 1. Bundesliga bei den Damen und bis zur 2. Bundesliga bei den Herren können die Aktiven in Berlin spielen und Tischtennissport erleben.
    Der Verband organisiert für alle Altersklassen diverse Veranstaltungen und Turniere.
    Der BTTV vertritt die Interessen der Vereine im übergeordneten Deutschen Tischtennis Bund (DTTB) sowie im Landessportbund Berlin.
    