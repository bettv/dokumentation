# Webseite und Mails

- Mails sind hier dabei, weil sie meist im Hosting inbegriffen sind


## Webseite

- https://www.bettv.de/
- Außendarstellung des Verbands, zentrale Anlaufstelle
- Informationen über den Verband
- Newsletter vs. News
	- wann was?
- Newsletter
	- öffentlich
	- verbandsintern
	- Presseverteiler
	- subscribe für News, auch themenbezogen
	- Mail, RSS
- Dokumentation
	- Verbandsstruktur
	- How-To
		- Formulare
			- Turniere, RACE
		- Turnierorga
			- Ausschreibung
			- Dokumentation/Dokumente
			- Technik (Software, Video)
			- Finanzen
			- Auf- und Abbau-Teams
	- Archiv
	- Sichtbarkeit
		- öffentlich
		- Verband
		- intern
	- Editieren nach Anmeldung
- verbandsinternes Forum
	- streng moderiert
- Geschichte
- Dokumentablage
	- Bilder
	- Protokolle
	- eventbasiert
		- Info
		- Ausschreibung
		- Ergebnisse
- Themenbereiche
	- Jugendwebseite
		- Vereine mit Jugendarbeit
		- Klassifizierung der Jugendarbeit
	- Kindeswohl
	- Senioren
	- Menschen im Verband
		- kleine Portraits
	- Orgateams
	- Ansprechpartner:innen
	- Technik
	- ...
- Vereinskarte mit Detailinformationen und Suche/Filter
	- woher kommen die Vereinsdaten?
		- tt-live
	- https://pingpongmap.net
	- https://sozialhelden.de/wheelmap/
	- Filter
		- Jugendarbeit
		- Damen
		- Barrierefreiheit
		- Probetraining
		- Senior:innen
	- Infoseite pro Verein
		- Webseite
		- tt-Live, clickTT
		- Funktionäre
		- Spielorte
		- Angebote, Trainingszeiten
- Veranstaltungskalender (abonnierbar themenbezogen)
- Turnierdokumentation (Ausschreibungen, Livestream und Ergebnisse)
- Livestreams
- Social-Media-Inhalte
	- Kanäle
		- Facebook
		- Youtube
		- TikTok
		- twitch
		- Podcasts
- Ergebnisse (Spielbetrieb, Turniere, überregional)
- Veranstaltungsbezogene Seiten (temporär)
	- TTT
	- Special Olympics
	- ...
- Vereinsservice
	- Unterstützung bei Software
	- Geschäftsstelle
- Ereignisse
	- Bundesligen
	- neue Spieler:innen
	- Turniere
- Aktionen
	- Aufruf
	- Vorbereitung
	- Durchführung
	- Nachbereitung
	- Archivierung
- Umfragen


## Datenquellen

- Verband selbst
- TT-Live
- clickTT
- nuLiga
- Vereine
- DTTB?
- ITTF?
- ...?


## Rahmenbedingungen

- barrierefrei
- ohne JS
- mobil
- PDF-Viewer
- mehrsprachig
	- Deutsch
	- Englisch
	- Leichte Sprache


## Offene Daten

*Grundlage für viele Funktionen der Webseite*

- Turniere:
	- Tage
	- Zeiten
	- Orte
- Vereine:
	- Name
	- Ort (geoloc)
	- Angebot:
		- Senior:innen
		- Jugend:
			- Altersklassen
		- Gesundheitssport
		- Frauen
		- Para
		- Special Olympics
- Trainingszeiten
- Funktionäre
- Downloads
- Ergebnisse
- Spielpläne


## Mails

- Funktionsmailadressen
	- praesidium
	- praesident, praesidentin - zeigt auf gleiches Postfach
	- vpsport, ...
	- sportausschuss, ...
	- aktivensprecher, aktivensprecherin
	- vsro, vsrausschuss, ...
	- ...
- keine namentlichen Mailadressen, da diese bei Ausscheiden verfallen
	- Regelung für private E-Mails
- oder namentliche Mailadressen
	- Regelungen für Zugriff nach Ausscheiden


## Wordpress

- Datenbank, Formulare
	- WP Data Access <https://wordpress.org/plugins/wp-data-access/>
- Wissensmanagement
	- Knowledge Base for Documentation, FAQs with AI Assistance <https://wordpress.org/plugins/echo-knowledge-base/>
	- weDocs – Knowledgebase and Documentation Plugin for WordPress <https://wordpress.org/plugins/wedocs/>
	- Knowledge Base documentation & wiki plugin – BasePress Docs <https://wordpress.org/plugins/basepress/>
- Newsletter
- Personenverwaltung
	- ausscheidende Redakteure
- Templates, Sitebuilder
- eigene Plugins
- Karten
	- Leaflet Map <https://wordpress.org/plugins/leaflet-map/>
	- Extensions for Leaflet Map <https://wordpress.org/plugins/extensions-leaflet-map/>

---

## Kommunikation - Übernahme alter Ideen, noch nicht eingearbeitet

## Rahmenbedingungen

- niedrigschwellig
- mehrsprachig (de, en, ?)

## statische Informationen

- Verbandsstruktur
	- Funktionäre
		- Aufgaben, Arbeitsumfang
	- Ausschüsse
	- Ressorts
- Themen und Ansprechpartner für Themen
	- Kinderschutz
	- Leistungssport
	- Leistungszentrum
	- Frauen
	- Senior:innen
	- Spielbetrieb
	- Schiedsrichter:innen
	- DTTB, LSB, Land Berlin
- Vereine
	- filterbar nach Geschlecht, Sprache, Angebot, Barrierefreiheit, ...
	- Karte
	- Verlinkungen
	- Infoseite pro Verein mit Kontaktmöglichkeit oder Link
- How-Tos
	- Turnierorga
- Geschichte

### Technik

- Webseite
- wenn möglich, strukturierte Informationen per REST-API

## dynamische Informationen

- News
- Newsletter abonnierbar
	- alle
	- themenbezogen
- Pflichtkommunikation an Vereine
	- Newsletter oder E-Mail
	- Einladungen Verbandstag o.ä.
	- Mitteilungen des Präsidiums
- Termine
	- themenbezogen
	- filterbar
	- abonnierbar

### Technik

- RSS-Feed
- Newsletter-Mailings
- Kalender mit abonnierbarem ical


## Sport- und Spielbetrieb

- Berliner Ligen
- Turniere
- Freizeitliga
- Kirchenliga
- Freizeitsport

### Technik

- wenn möglich, per REST-API


## Vernetzung

- Diskussion
- gegenseitige Hilfe

### Technik

- Mastodon
- Twitter
- Facebook
- BTTV-Webseite
