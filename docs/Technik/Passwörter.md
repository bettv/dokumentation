# Passwörter

Die persönlichen Passwörter der jeweiligen Personen liegen in deren Verantwortung.

Zur Verwaltung wird KeePassXC empfohlen: betriebssystemübergreifend, Apps für Handys vorhanden.

Alle Accounts, insbesondere Mail- und Serveraccounts müssen unterschiedliche Passwörter erhalten.
Damit kann ein geknacktes Passwort nicht zur Übernahme anderer Accounts genutzt werden.

Es müssen komplexe Passwörter mit mindestens 24 Zeichen genutzt werden.
Komplex heißt:nicht ratbar, verschiedene Zeichenarten.


## Root-Admin-Passwörter

1. Mailadmin
2. Webseitenadmin
3. NAS-Admin

Die Admin-Passwörter werden in einer KeePassXC-Datei verwaltet, die durch Passwort und Schlüsseldatei abgesichert ist.

Sie liegt auf dem Server unter:

`Tauschlaufwerk-GF/007-Präsidium-Finanzen-Verträge/Passwörter/BeTTV.kdbx`

Zugriff haben:

- Präsident*in
- Datenschutzbeauftragte:r

Für den Notfall werden Passwort und Schlüsseldatei beim Notar hinterlegt.


### Turnusmäßige Passwortänderungen

Die Admin-Passwörter sowie das Passwort und die Schlüsseldatei für die KeePassXC-Datei  werden nach jeder Präsidiumswahl neu erzeugt, also alle zwei Jahre.

Das sollte ausreichen, um Missbrauch einzudämmen und ist lang genug, um nicht schlechte Passwörter zu verwenden.

Die Admin-Passwörter werden mit dem KeePassXC-Generator erzeugt und müssen mindestens 24 Zeichen lang sein.


## Passwörter zur normalen Administration

Zur "täglichen" Administration werden personalisierte Accounts eingerichtet, die die Rolle Admin bekommen.

Dies sind zunächst die Mitarbeiter:innen der Geschäftsstelle.

Weitere Admin-Accounts können auf Anfrage eingerichtet werden, bei Unklarheiten entscheidet das Präsidium.

Die Accounts werden mit Ausscheiden der Person aus Amt, Funktion oder Stelle deaktiviert und, sofern möglich, nach angemessener Zeit gelöscht.


## Mitarbeiter:innen-Passwörter

- ein Passwortmanager wird empfohlen
    - KeePassXC


## WLAN-Passwörter

- Mitarbeiter:innen-Passwort
    - ungleich dem Mail- oder Serverpasswort
- Gast-Passwort 24 Zeichen lang, nur Zahlen und Kleinbuchstaben
    - damit etwas schwächer aber einfacher einzutippen ohne Passwortmanager
    - deswegen jährliche Änderung
    - Ausdruck in Besprechungsraum hinterlegen