# Geschäftsstelle

## Besprechungsraum

- WLAN für Gäste
- Beamer nach Anmeldung oder großer Monitor
- Kleinstrechner für Vorführungen und Videokonferenzen mit Kamera und Mikrofon
    - Windows (wegen Videokonferenzen)
    - PDF-Viewer vorinstalliert
    - Powerpoint ebenfalls
- Whiteboard mit Kreidestiften

## Mitarbeiter:innen

- Laptop mit Display eigener Wahl
- mindestens ein, maxmial zwei Monitore in der Geschäftsstelle
- Tastatur und Maus in Geschäftsstelle, auf Wunsch auch für HomeOffice
- VPN-Zugang zum Geschäftsstellenserver voreingerichtet

## Server

- Synaptic-NAS im Druckerraum im Serverschrank
- Verbindung über WLAN in der Geschäftsstelle
- Internetanbindung
- von außen über VPN erreichbar