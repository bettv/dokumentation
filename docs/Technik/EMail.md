# E-Mail

!!! warning "ToDo"
    - fehlende Ausschüsse nachtragen
    - weitere E-Mails erfragen
    - Backup-Konzept mit Anforderungen und derzeitigem Stand abgleichen
    - konkrete E-Mailadressen und Empfänger dokumentieren und hier verlinken

!!! tip ""
    Mailkonzept mit Erläuterungen.
    Die konkret aktuellen E-Mails und deren Empfänger finden Sie ...

Zunächst wird das Mailkonzept des BTTV beschrieben.
Erklärungen zu den Termini und Ideen folgen unten.

## Mailkonzept des BTTV

### Funktionsmailadressen mit Postfächern

Für folgende Funktionen werden Funktionsmailadressen als Mailpostfächer eingerichtet:

- alle Mitglieder des Präsidiums
- Verbandstrainer
- Jugendsprecherin und Jugendsprecher

Es wird versucht, die Adressen möglichst geschlechtsneutral zu benennen, damit die angestrebte Stabilität der Funktionsmailadressen erzielt wird.

Postfach | Name
--- | ---
praesi | Präsident:in
vpsport | Vizepräsident:in Sport
vpfinanzen | Vizepräsident:in Finanzen
vpoeffentlichkeit | Vizepräsident:in Öffentlichkeitsarbeit
vpjugend | Vizepräsident:in Jugend
vpbildung | Vizepräsident:in Bildung
verbandstraining | Verbandstrainer:in
jugendsprecher | Jugendsprecher
jugendsprecherin | Jugendsprecherin

### Funktionsmailadressen mit Postfächern und Weiterleitung

Für folgende Gruppen werden Funktionsmailadressen als Mailpostfächer mit Weiterleitung eingerichtet:

- Präsidium
- Geschäftsstelle

Das sorgt dafür, dass alle eingehenden E-Mails im Archiv vorhanden sind.
Ausgehende E-Mails werden dem Postfach nur zugeordnet, wenn sie vom Postfach gesendet wurden.

Postfach | Name | Zusätzliche Weiterleitung an
--- | --- | ---
praesidium | Präsidium | alle Präsidiumsmitglieder
geschaeftsstelle | Geschäftsstelle | Mitarbeiter:innen der Geschäftsstelle

### Funktionsmailadressen mit Weiterleitung

Für folgende Gruppen und Personen werden Funktionsmailadressen als Weiterleitungen eingerichtet:

- Ausschüsse
- Spielleiter
- VSRO

Postfach | Name | Weiterleitung an
--- | --- | ---
sportausschuss | Sportausschuss | Mitglieder des Ausschuss'
jugendausschuss | Jugendausschuss | Mitglieder des Ausschuss'
seniorenausschuss | Seniorenausschuss | Mitglieder des Ausschuss'
vsra | Verbandsschiedsrichterausschuss | Mitglieder des Ausschuss'
spielleiter | Spielleiter | Mitglieder des Ausschuss'
vsro | Verbandsschiedsrichterobmann | VSRO

### Persönliche Mailadressen mit Postfach

Für folgende Personen werden persönliche Mailadressen als Mailpostfächer eingerichtet:

- Mitarbeiter:innen der Geschäftsstelle
- amtierende:r Präsident:in für persönliche E-Mails
- auf Wunsch, Genehmigung durch Präsidium

Das Verschieben von Mails zwischen den Funktions- und persönlichen Accounts obliegt den Personen.
Dies betrifft sowohl eingehande als auch ausgehende E-Mails.

Es ist dafür zu sorgen, dass offizielle Verbands-E-Mails, insbesondere mit Auswirkungen auf die Verbandsarbeit, im Funktionspostfach landen.

### Noch zu klären

- ehrenpraesident
- verbandsgerichtsvorsitz
- kindeswohl
- welche Ausschüsse fehlen noch


### Sicherungskonzept (Abfragen, wie es derzeit beim Anbieter läuft)

Die E-Mailpostfächer des BTT werden durch dem Maildienstleister gesichert.
Die Sicherung erfolgt täglich, ein Zugriff ist durch Mailadmins nach Zustimmung des Präsidiums möglich.

Eine langfristige Sicherung der E-Mails erfolgt ...

Der BTTV zieht einmal jährlich ein Backup aller Mailpostfächer als verschlüsselte zip-Datei.
Der Schlüssel wird in der Admin-Schlüsseldatei hinterlegt.
Das Backup wird auf dem BTTV-Server abgelegt.

### Löschkonzept

Funktionsmailpostfächer werden nicht gelöscht.
Sie gehen bei Übergabe der Funkktion auf den/die Nachfolger:in über.

Ausnahme: 
Entfällt eine Funktion, so werden deren E-Mails an die funktionsähnlichste Funktion übergeben (kopiert), archiviert und das Postfach geschlossen und nach einem Jahr zum Jahresende gelöscht.
Existiert keine funktionsähnlichste Funktion, dann übernimmt der/die Präsident:in.

Persönliche Postfächer werden nach Entfall des Grunds für das Postfach (Ausscheiden, Abwahl, Entzug durch Präsidium) geschlossen.
Es wird ein Backup angefertigt, das der Person bis zur Löschung des Postfachs zum Download angeboten wird.
Das Postfach sowie das Backup werden nach einem Jahr zum Jahresende gelöscht.


## Funktions-Mailadressen vs. persönliche Mailadressen

### Funktions-Mailadressen

sind Mailadressen, bei denen die Funktion Teil der Mailadresse ist, also ˋgeschaeftsstelle@bettv.deˋ oder ˋpraesident@bettv.deˋ

Vorteile

- bei Wechsel keine neuen Mailadressen (für Webseite gut aber auch für Newsletter, andere Mailverteiler etc.)
- man muss die Personen nicht namentlich kennen, die dahinter stehen
- Mails gehen durch Wechsel nicht verloren, da sie von den Nachfolger:innen gelesen werden können

Nachteil

- keine persönlichen Mails möglich, die nicht von den Nachfolgern gelesen werden können

### Persönliche Mailadressen

sind Mailadressen, bei denen der Name Teil der Mailadresse ist, also ˋekkart.kleinod@bettv.deˋ

Vor- und Nachteile sind entgegengesetzt zu den oben genannten.

Da die E-Mails personalisiert sind, dürfen sie nicht ohne Weiteres von anderen, insbesondere Nachfolgern in einer Position, gelesen werden.
Dies ist nur im Missbrauchs- oder Notfall möglich.


## Mailpostfächer und Weiterleitungen

**Mailpostfächer** sind eigene Postfächer in der Verwaltung des BTTV.
Sie werden beim Maildienstleister eingerichtet und können per Webmail oder IMAP genutzt werden.
Ihre Speicherung und Sicherung übernimmt der Maildienstleister des BTTV.

**Weiterleitungen** leiten E-Mails, die an eine Adresse geschickt werden, an eine oder mehrere andere E-Mailadressen weiter.
Diese E-Mails werden nicht zwischengespeichert.


## Personen und Gruppenmailadressen

**Personenmailadressen** sind Mailadressen, deren Empfänger eine bestimmte Person ist.
Hier ist meist aus der Mailadresse erkennbar, welche Person die E-Mail empfängt.

**Gruppenmailadressen** sind Mailadressen, deren Empfänger eine Gruppe von Personen ist.
Hier muss der Empfängerkreis öffentlich dokumentiert werden, da er aus der Mailadresse direkt nicht erkennbar ist.


## Missbrauchs- oder Notfall

Ein **Missbrauchsfall** tritt auf, wenn die Mailadresse für rechtswidrige Zwecke missbraucht wird.
Diese sind durch Anzeige bei der Polizei zu dokumentieren.
Im Rahmen der Beweissicherung ist ein Zugriff durch die Mailadmins und die Polizei oder Staatsanwaltschaft im Vier-Augen-Prinzip (Polizei/Staatsanwalt und Admin) erlaubt.  

Ein **Notfall** tritt auf, wenn Gefahr für Leib und Leben oder ein erheblicher Schaden für denVerband abgewehrt werden muss.
Der Notfall ist schriftlich zu dokumentieren, z.B. per E-Mail.
Ob ein Notfall eingetreten ist, entscheidet das Präsidium mit einfacher Mehrheit der anwesenden Stimmen, eine Abstimmung per E-Mail ist möglich, dann sind alle Präsidiumsmitglieder als "anwesend" zu zählen.
Der Zugriff erfolgt im Vier-Augen-Prinzip durch zwei Mitglieder des Präsidiums oder ein Mitglied des Präsidiums und eine:n Mitarbeiter:in der Geschäftsstelle.

Die Person,(en) die das betroffene Postfach besitzen, ist/sind nach Zugriff zu informieren, mindestens per E-Mail. 
