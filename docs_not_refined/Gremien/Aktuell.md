# Aktuell

- <https://bettv.tischtennislive.de/default.aspx?L1=Public&L2=Funktionaere>
- <https://www.bettv.de/verband/praesidium-funktionaere/>

## Ehrenpräsident:in

Jörg Dampke

## Präsident:in

Ekkart Kleinod

## Vizepräsident:in Jugend

Jörg Kersten

## Vizepräsident:in Öffentlichkeitsarbeit

Steffen Zeidler

## Vizepräsident:in Sport

-

## Vizepräsident:in Finanzen

Achim Plötz

## Sportausschuss

Vorsitzende:r (Vizepräsident:in Sport)
: -

Beisitzer:innen
: - Jurij Richter
	- Referent:in Damensport Andrea Pfitzner
	- Referent:in Mannschaftssport Andreas Ahlers

## Jugendausschuss

Beisitzer:innen
: - Oliver Bertram
	- Daniela Standke
	- Achim Plötz
	- Jannik Jung

## Jugendspielausschuss

Vorsitzende:r
: Achim Plötz

## Spielausschuss

Vorsitzende:r (Referent:in Mannschaftssport)
: Andreas Ahlers

Spielleiter:innen Jugend
: - Harry Raeck
	- Norbert Standke
	- Achim Plötz

Spielleiter:innen Erwachsene
: - Daniela Standke
	- Patricia Standke
	- Axel Subklewe
	- Jörg Kersten
	- Stefan Fricke
	- Joachim Klein
	- Frank Schröter

Aktivensprecher:in Erwachsene
: Eric Osbar


## Referent:in für Schulsport

Bettina Engel

## Schulsportausschuss

Beisitzer:innen
: - Gudrun Engel
	- Ron Affeldt

## Referent:in

Andrea Pfitzner

## Referent:in Mannschaftssport

Andreas Ahlers

Stellvertreter:in
: - Daniela Standke

## Aktivensprecher:in Erwachsene

Eric Osbar

## Freizeit- und Breitensportausschuss

Vorsitzende:r (Referentin für Freizeit- und Breitensport)
: unbesetzt

Beisitzer:innen
: - Michael Paszkowiak
	- Andreas Kurth

## Spielleiter Freizeitliga	Rainer Gusewski
## Referent Seniorensport / Mg. SportA	Peter Wode
## Spielleiter Senioren	Thomas Schlegelmilch
## Schiedsrichterobmann / Mg. SportA	Ekkart Kleinod
## Vertreter Schiedsrichterobmann	Martin Becker
## Beisitzer:in Schiedsrichterausschuss	Alexander Ohle
## Beisitzer:in Lehrausschuss	Christopher Przydatek
## Beisitzer:in Lehrausschuss	Marcel Schwartz
## Vorsitzender Gesundheitssportausschuss	Margrit Howald
## Beisitzer:in Gesundheitssportausschuss	Jürgen Schäffner
## Vorsitzender Verbandsgericht	Ina Tschirsky
## Beisitzer:in Verbandsgericht	Rainer Lamprecht
## Beisitzer:in Kontrollausschuss	Andreas Wattenberg
## Kassenprüfer	Frank Mineif
## Kassenprüfer	Dieter Krüger
## Kassenprüfer	Horst Hennig
## Ehrenmitglied	Jutta Trapp
## Ehrenmitglied	Manfred Groß
## Ehrenmitglied	Eberhard Große
## Ehrenmitglied	Walter Zickert
## Anti-Doping-Beauftragter	Michael Althoff
## Kindeswohlbeauftragter	Friedrich Hartmann
## Datenschutzbeauftragter	Achim Plötz
## Ständiger Gast des Verbandstags	FTT Betriebssportverband
## Geschäftsstelle	BeTTV Karteistelle
## Beisitzer:in Jugendausschuss	Irina Palina
