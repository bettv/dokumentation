# Rechnungsprüfrung

[1, S. 196 f.]

- "Kassenprüfung", "Revision" oder "Prüfung der Geschäftsführung"
- Name legt RIchtung der Prüfung fest
- Verbandstag legt Gegenstand und Umfang der Prüfung fest
- üblich: 2 Personen
- unvermutet und unangemeldet Geschäftsführung alle Vereinsorgane prüfen
- dürfen nicht behindert werden, auch nicht durch Schweigen
- da Vereinsorgane geprüft werden, dürfen Prüferinnen ihnen nicht angehören
