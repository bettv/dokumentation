# Rechtsgrundlagen

!!! tip ""
	Kleine Sammlung, nicht vollständig, eher praktische Dinge.


## Gesetze

*wichtige Auszüge*

- Bürgerliches Gesetzbuch BGB, §§ 21 – 79, [BGB](https://www.gesetze-im-internet.de/bgb/index.html)
- Vereinsregisterverordnung VRV, §§ 1 – 33, [VRV](https://www.gesetze-im-internet.de/vrv/index.html)
- Abgabenordnung AO, §§ 51 – 68, [AO](https://www.gesetze-im-internet.de/ao_1977/index.html)

## Quellen

- [1] Der eingetragene Verein; Sauter, Schweyer, Waldner; 20. Auflage, C.H. Beck
- [2] Satzung des BTTV vom 07.09.2020
