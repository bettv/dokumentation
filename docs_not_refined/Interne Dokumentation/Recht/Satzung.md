# Satzung

[1, S. 74 ff.]

- Mindestinhalt §§ 57 und 58 BGB

empfohlen

- Art der Einladung zur Mitgliederversammlung
- Aufgaben der Vereinsorgane
- Stimmenverteilung auf Vereine

## Änderung

- bei Einladung Entwurf anhängen
- Mehrheit zur Änderung der Satzung
	- ohne Angabe = 3/4 der abgegebenen Stimmen, § 33 BGB
	- abgegebene Stimmen = ja und nein; Enthaltungen und ungültige nicht zählen
	- Satzung kann anders regeln
- einzelne Satzungsvorschrift
	- jeden einzeln abstimmen
	- nur dieser kann geändert (und diskutiert) werden, nicht der Rest
- gesamte Satzung
	- viele Änderungen
	- gesamte Satzung kann diskutiert und geändert werden, nicht nur angegebene Vorschriften
	- daher taktisch überlegen, wie groß die Diskussion sein soll
- gültig **nach** Eintragung in Vereinsregister
