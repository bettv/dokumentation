# Präsidium

[1, S. 139 ff.]

- "Vorstand" oder "Vereinsvorstand"
- zwingend notwendig
- rechtlich: gesetzlicher Vertreter des Vereins
	- nach Wahl in Vereinsregister nötig
	- direkt nach Wahl im Amt
- mehrere Mitglieder = mehrgliedriger Vorstand
- alle Mitglieder müssen befugt zur Vertretung des Vereins sein
- keine anderen Mitglieder sind zur Vertretung befugt
	- also auch nicht Geschäftsführerin zusammen mit 1 anderen Vorstandsmitglied

## Entlastung

- Verbandstag erklärt, Billigung der Geschäftsführung des Vorstands als gesetz- und satzungsgemäß
- Verzicht auf Bereicherungs- und Schadensersatzansprüche
- Grundlagen
	- Berichte
	- Vorlagen
	- Rechenschaftsberichte
	- Jahresabschluss
- kein Stimmrecht des Vorstands
