# Mitgliederversammlung

[1, S. 72 ff., S. 90 ff.]
[2, § 10]


## Durchführung

### Einladung

- satzungsgemäße Form (Schriftform = Brief sowie E-Mail, Fax nach Änderung § 126 BGB)
- satzungsgemäßer Turnus
- Tagesordnung
	- Satzungsänderungen: mit Anhang der Änderungen
	- Hinweis auf Antragsfrist
- Anträge vor Veröffentlichungsfrist
- Fristen laut Satzung
	- jedes Jahr im Mai oder Juni
	- spätestens sechs Wochen vorher schriftlich einberufen, außerordentlicher Verbandstag 14 Tage
	- Anträge auf Änderung der Satzung und der Ordnungen spätestens 4 Wochen vorher einreichen
	- Anträge spätestens 2 Wochen vorher veröffentlichen

### Vor Sitzungsbeginn

- Versammlungsleiterin
	- Person
		- Vorrecht: Satzungsbestimmung
		- ohne Regelung: Vorstand in Reihenfolge Präsidentin, Stellvertretende, dann egal
	- Leitung der Versammlung
	- Hausrecht
- Protokollführerin
- Stimmenzählerinnen

### Sitzung

- förmliche Eröffnung
	- ab da rechtserhebliche Bedeutung
	- Beginn der Ordnungsgewalt der Versammlungsleiterin
- Begrüßung
- Feststellung Beschlussfähigkeit
- Bekanntgabe Tagesordnung
	- Abweichung der Reihenfolge
		- nach pflichtgemäßem Ermessen
		- möglichst durch Versammlung bestätigen lassen
		- auf Antrag zur Abstimmung stellen
	- neue Punkte auf die Tagesordnung
		- darf nicht erfolgen
		- es sei denn, Satzung erlaubt das
- Erledigung der Tagesordnung
- Wortmeldungen
	- nach Reihenfolge (davon darf abgewichen werden)
	- Redezeitbegrenzung möglich
	- Entziehung des Worts nach Ermahnung
- Beschlüsse
	- endgültig, sie können nicht noch einmal behandelt werden (Mitglieder können gegangen sein)
	- zurückgezogene Anträge können aufleben, wenn kein Beschluss dazu gefasst wurde
	- Verkündung der Beschlüsse zwingend
- Ausschluss oder Saalverweis
	- nach schwächeren Maßnahmen und Androhung der Maßnahme
	- bei nichtberechtigten Personen
	- Störungen, Beleidigungen
	- Festhalten im Protokoll
- Aufnahmen
	- vorher ankündigen
	- Beschluss der Versammlung kann verbieten
	- einzelne Rednerinnen können Nichtaufnahme ihres Beitrags verlangen
- Unterbrechung der Versammlung (ins Protokoll)
- förmliche Schließung der Versammlung


### Nachbereitung

- Beurkundung von Beschlüssen
- Eintragungen in Vereinsregister
	- mindestens mit unterschriebenem Protokollauszug
- Protokollveröffentlichung


## Anwesenheitsliste

- anwesende Personen mit Uhrzeit und Stimmenzahl


## Beschlüsse

- Wirkung
	- Satzungsänderungen: nach Eintragungen Vereinsregister
	- alles andere: sofort (auch Wahlen)
- Art der Abstimmung
	- Möglichkeiten
		- durch Zuruf (Karten)
		- schriftlich
		- schriftlich-geheim (verdeckte Stimmzettel)
		- Kugelung (Ballotage) - weiße und schwarze Kugeln
	- Auswahl: Satzung oder Versammlungsleiterin
	- Befragung der Versammlung möglich aber nicht nötig
- genaues Abstimmungsergebnis protokollieren
- Auszählung: ja und nein zählen, Enthaltungen subtrahieren
	- bei knappen oder wichtigen Beschlüssen: alles auszählen
- Mehrheiten
	- einfache Mehrheit: mehr ja als nein, Enthaltung nicht gezählt
	- absolute Mehrheit: meist einfache Mehrheit, Begriff vermeiden oder in Satzung definieren
	- qualifizierte Mehrheit: mehr als einfache Mehrheit, wird festgelegt, z.B. 2/3
	- relative Mehrheit: die meisten Stimmen, insbesondere bei mehrere Kandidatinnen
	- abgegebene Stimmen = ja und nein; Enthaltungen und ungültige nicht zählen
- BeTTV laut Satzung
	- Auflösung: 9/10 der abgegebenen Stimmen
	- Satzungsänderung: 2/3 der abgegebenen Stimmen
	- alles andere: relative Mehrheit


## Protokoll

1. Ort, Tag, Stunde
2. Versammlungsleiterin und Protokollführerin
3. Zahl erschienener Mitglieder
4. Feststellung, dass ordnungsgemäß einberufen
5. Tagesordnung mit Feststellung, dass bei Einberufung mitgeteilt
6. Feststellung Beschlussfähigkeit
7. Anträge
	1. Inhalt/Titel
	2. Art der Abstimmung
	3. Abstimmungsergebnis (ja, nein, Enthaltungen, ungültige Stimmen)
	4. Feststellung des Beschlusses
8. Wahlen
	1. Position
	2. Namen der Gewählten
	3. Annahme der Wahl
9. wesentliche Wortbeiträge (inhaltlich)
10. Unterschrift Protokollführerin und Versammlungsleiterin

### Anhänge

- Tagesordnung
- Anträge
- Anwesenheitsliste
