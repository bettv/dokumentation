# Finanzen

!!! tip ""
    Kleine Sammlung, nicht vollständig, eher praktische Dinge.


## Rücklagen

- Ende eines Jahrs ist entweder Geld übrig oder nicht
- wenn Geld übrig ist, sind das die Rücklagen des Verbands
- damit kann man auf zwei Arten umgehen:
    1. festlegen (deklarieren), wofür es in Zukunft ausgegeben wird
        - das können alles oder Teile sein
        - das sind "feste" Rücklagen, die deklariert werden
        - sie dürfen dann nur für die angegebenen Zwecke ausgegeben werden
        - bei Fördertöpfen werden die festen Rücklagen meist nicht berücksichtigt, da sie ja verplant, also quasi ausgegeben sind
    2. nichts festlegen (deklarieren)
        - dann steht das Geld normal zur Verfügung
        - bei Fördertöpfen darf meist der Überschuss eine bestimmte Summe nicht übersteigen, sonst braucht man ja keine Förderung
