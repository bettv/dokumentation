# Ein Turnier organisieren

## Vorlagen


## Vorgehen

- Sportausschuss informieren
- OSR und, falls nötig, Tischschiris beim Verbandsschiedsrichterausschuss VSRA anfordern


## Zu veröffentlichende Informationen

- Ankündigung
- Ausschreibung
- Ergebnisse
- Bericht


## Ergebnisdokumente

- Ergebnisse
- Inhaltlicher Bericht
- OSR-Bericht
- Gelerntes


## Offene Fragen

- Eintrittsgelder
- Verpflegung
- Geld für Veranstalter
- mktt-Nutzung
- Halle, Tischtransport, Netze, Bälle
